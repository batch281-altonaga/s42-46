const router = require("express").Router();
const buildController = require("../controllers/buildController");
const auth = require("../middleware/auth");

// Get all builds (admin only)
router.get("/", auth.verifyToken, buildController.getAllBuilds);

// Retrieve a build by id
router.get("/:id", auth.verifyToken, buildController.getBuildById);

// Create a build (order)
router.post("/", auth.verifyToken, buildController.createBuild);

// Add a part to a build
router.patch("/:id", auth.verifyToken, buildController.addPartToBuild);

// Update part quantity in a build
router.put("/:id", auth.verifyToken, buildController.updatePartQuantity);

// Remove a part from a build
router.put(
  "/:id/remove",
  auth.verifyToken,
  buildController.removePartFromBuild
);

// Delete a build (admin only)
router.delete("/:id", auth.verifyToken, buildController.deleteBuild);

module.exports = router;
