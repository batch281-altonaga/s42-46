const Build = require("../models/build");
const Part = require("../models/part");
const auth = require("../middleware/auth");

// Get all Builds (admin only)
module.exports.getAllBuilds = async (req, res) => {
  try {
    const user = auth.decode(req.headers.authorization);

    if (!user.isAdmin) {
      return res.status(401).send("Unauthorized");
    }

    await Build.find().then((result) => {
      res.status(200).send(result);
    });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

// Get a build by id
module.exports.getBuildById = async (req, res) => {
  try {
    const user = auth.decode(req.headers.authorization);

    if (!user.isAdmin) {
      return res.status(401).send("Unauthorized");
    }

    // Find the build

    const build = await Build.findOne({
      _id: req.params.id,
    });

    if (!build) {
      return res.status(404).send("Build not found");
    }

    return res.status(200).send(build);
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

// Create build
module.exports.createBuild = async (req, res) => {
  try {
    const user = auth.decode(req.headers.authorization);

    const { userId, parts } = req.body;

    const partIds = parts.map((part) => part.partId);

    // Find all parts from the database
    const partList = await Part.find({ _id: { $in: partIds } });

    const buildParts = [];

    // Loop through all parts from the request body
    for (let i = 0; i < parts.length; i++) {
      const partId = parts[i].partId;
      const partQuantity = parts[i].quantity;

      // Find the part in the database
      const part = partList.find((part) => part.id === partId);

      // If part not found
      if (!part) {
        return res.status(404).send("Part not found");
      }

      // Check if the part is in stock
      if (!part.isAvailable) {
        return res.status(400).send("Part is not available");
      }

      buildParts.push({
        partId: part.id,
        quantity: partQuantity,
        price: part.price,
      });
    }

    // Create a new build
    const build = new Build({
      userId,
      parts: buildParts,
    });

    // Calculate total amount
    const totalSum = buildParts.reduce((sum, part) => {
      return sum + part.price * part.quantity;
    }, 0);

    // Round off to 2 decimal places
    build.totalAmount = Math.round(totalSum * 100) / 100;

    return await build.save().then((result) => {
      res.status(201).send(result);
    });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Add a part to a build
module.exports.addPartToBuild = async (req, res) => {
  try {
    const user = auth.decode(req.headers.authorization);

    // Find the build
    const build = await Build.findOne({
      userId: user.id,
      _id: req.params.id,
    });

    // If build not found
    if (!build) {
      return res.status(404).send("Build not found");
    }

    const { parts } = req.body;

    const partIds = parts.map((part) => part.partId);

    // Find all parts from the database
    const partList = await Part.find({ _id: { $in: partIds } });

    // Loop through all parts from the request body
    for (let i = 0; i < parts.length; i++) {
      const partId = parts[i].partId;
      const partQuantity = parts[i].quantity;

      // Check if the part already exists in the cart
      const existingPart = build.parts.find(
        (part) => part.partId.toString() === partId
      );

      if (existingPart) {
        // Increase the quantity of the existing part
        existingPart.quantity += partQuantity;
      } else {
        // Find the part in the database
        const part = partList.find((part) => part._id.toString() === partId);

        if (!part) {
          // Check if part is not found
          return res.status(404).send(`${partId} not found`);
        }

        // Check if part is available
        if (!part.isAvailable) {
          return res.status(400).send(`${partId} is not available`);
        }

        // Add the new part
        build.parts.push({
          partId: part._id,
          quantity: partQuantity,
          price: part.price,
        });
      }
    }

    // Calculate total amount
    const totalSum = build.parts.reduce((sum, part) => {
      return sum + part.price * part.quantity;
    }, 0);

    // Round off to 2 decimal places
    build.totalAmount = Math.round(totalSum * 100) / 100;
    const updatedBuild = await build.save();
    res.status(200).send(updatedBuild);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Update part quantity
module.exports.updatePartQuantity = async (req, res) => {
  try {
    const user = auth.decode(req.headers.authorization);

    // Find the build
    const build = await Build.findOne({
      userId: user.id,
      _id: req.params.id,
    });

    if (!build) {
      return res.status(404).send("Build not found");
    }

    // Retrieve part from the request body
    const partToBeUpdated = req.body.partId;
    const newQuantity = req.body.quantity;

    // Retrieve part from build
    const part = build.parts.find(
      (part) => part.partId.toString() === partToBeUpdated
    );

    if (!part) {
      // Check if part is not found
      return res.status(404).send(`${partToBeUpdated} not found`);
    }

    // Update the quantity
    part.quantity = newQuantity;

    // Update the total amount
    const totalSum = build.parts.reduce((sum, part) => {
      return sum + part.price * part.quantity;
    }, 0);

    // Round off to 2 decimal places
    build.totalAmount = Math.round(totalSum * 100) / 100;

    // Save the updated build
    const updatedBuild = await build.save();
    return res.status(200).send(updatedBuild);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Remove part from build
module.exports.removePartFromBuild = async (req, res) => {
  try {
    const user = auth.decode(req.headers.authorization);

    // Find the build
    const build = await Build.findOne({
      userId: user.id,
      _id: req.params.id,
    });

    if (!build) {
      return res.status(404).send("Build not found");
    }

    // Retrieve part from the request body
    const partToBeRemoved = req.body.partId;

    // Remove the parts from build
    for (let i = 0; i < build.parts.length; i++) {
      if (build.parts[i].partId.toString() === partToBeRemoved) {
        // Check if the part is not found
        build.parts.splice(i, 1);
      }
    }

    // Update the total amount
    const totalSum = build.parts.reduce((sum, part) => {
      return sum + part.price * part.quantity;
    }, 0);

    build.totalAmount = totalSum;

    // Save the updated build
    const updatedBuild = await build.save();

    res.status(200).send(updatedBuild);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

// Delete build
module.exports.deleteBuild = async (req, res) => {
  try {
    const user = auth.decode(req.headers.authorization);

    if (!user.isAdmin) {
      return res.status(403).send("Unauthorized. Must be an admin");
    }

    const { id } = req.params;

    return await Build.findByIdAndDelete(id).then((result) => {
      res.status(200).send(result);
    });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};
